import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { GroupSearchComponent } from './components/group-search/group-search.component';
import { NaviBarComponent } from './components/navi-bar/navi-bar.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { CreateGroupComponent } from './components/create-group/create-group.component';
import { UserOwnGroupsComponent } from './components/user-own-groups/user-own-groups.component';
import { HttpClientModule } from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    UserLoginComponent,
    NaviBarComponent,
    GroupSearchComponent,
    CreateGroupComponent,
    UserOwnGroupsComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
