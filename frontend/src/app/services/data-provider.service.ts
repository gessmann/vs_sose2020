import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { GroupDO } from '../types/groupDO';
import { FacultyDO } from '../types/facultyDO';
import { ResponseType } from '../types/response-type';
import { RequestResponse } from '../types/request-response.interface';
import {ModulesDO} from "../types/modulesDO";

@Injectable({
  providedIn: 'root'
})
export class DataProviderService {

  constructor(private httpClient: HttpClient) { }

  getGroupData(): Promise<RequestResponse<GroupDO[]>> {
    return new Promise((resolve, reject) => {
      this.httpClient.get<GroupDO[]>('http://localhost:8080/groups').toPromise().then(
        (groups: GroupDO[]) => {
          resolve({type: ResponseType.SUCCESS, payload: groups });
        }, (error: HttpErrorResponse) => {
          if(error.status === 0) {
            reject({type: ResponseType.CONNECTION_PROBLEM});
          } else if(error.status === 400) {
            reject({type: ResponseType.BAD_REQUEST});
          } else if(error.status === 500) {
            reject({type: ResponseType.INTERNAL_SERVER_ERROR});
          } else {
            reject({type: ResponseType.FAILURE});
          }
        });
    });
  }

  getFacultyData(): Promise<RequestResponse<FacultyDO[]>> {
    return new Promise((resolve, reject) => {
      this.httpClient.get<FacultyDO[]>('http://localhost:8080/faculties').toPromise().then(
        (faculties: FacultyDO[]) => {
          resolve({type: ResponseType.SUCCESS, payload: faculties});
        }, (error: HttpErrorResponse) => {
          if(error.status === 0) {
            reject({type: ResponseType.CONNECTION_PROBLEM});
          } else if(error.status === 400) {
            reject({type: ResponseType.BAD_REQUEST});
          } else if(error.status === 500) {
            reject({type: ResponseType.INTERNAL_SERVER_ERROR});
          } else {
            reject({type: ResponseType.FAILURE});
          }
        });
    });
  }

  getModulesData(): Promise<RequestResponse<ModulesDO[]>> {
    return new Promise((resolve, reject) => {
      this.httpClient.get<ModulesDO[]>('http://localhost:8080/module').toPromise().then(
        (modules: ModulesDO[]) => {
          resolve({type: ResponseType.SUCCESS, payload: modules});
        }, (error: HttpErrorResponse) => {
          if(error.status === 0) {
            reject({type: ResponseType.CONNECTION_PROBLEM});
          } else if(error.status === 400) {
            reject({type: ResponseType.BAD_REQUEST});
          } else if(error.status === 500) {
            reject({type: ResponseType.INTERNAL_SERVER_ERROR});
          } else {
            reject({type: ResponseType.FAILURE});
          }
        });
    });
  }

  createGroupData(group: GroupDO): Promise<RequestResponse<GroupDO>> {
    return new Promise((resolve, reject) => {
      this.httpClient.post<GroupDO>('http://localhost:8080/groups/new', group).toPromise().then(
        (group: GroupDO) => {
          resolve({type: ResponseType.SUCCESS, payload: group});
        }, (error: HttpErrorResponse) => {
          if(error.status === 0) {
            reject({type: ResponseType.CONNECTION_PROBLEM});
          } else if(error.status === 400) {
            reject({type: ResponseType.BAD_REQUEST});
          } else if(error.status === 500) {
            reject({type: ResponseType.INTERNAL_SERVER_ERROR});
          } else {
            reject({type: ResponseType.FAILURE});
          }
        });
    });
  }
}
