import {Component, Input, OnInit} from '@angular/core';
import {FacultyDO} from "../../types/facultyDO";
import {ModulesDO} from "../../types/modulesDO";
import {GroupDO} from "../../types/groupDO";
import {ActivatedRoute, Router} from "@angular/router";
import {DataProviderService} from "../../services/data-provider.service";
import {ResponseType} from "../../types/response-type";

@Component({
  selector: 'app-create-group-page',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {


  faculties: FacultyDO[];
  modules: ModulesDO[];
  groups: GroupDO[];
  selectedModule:ModulesDO;
  selectedFaculty:FacultyDO;
  newGroup: GroupDO;
  errorMessage: string;
  createdGroup: GroupDO;

  constructor(private router: Router, private route: ActivatedRoute,private dataService: DataProviderService) {}

  ngOnInit(): void {
    this.getAllFaculties();
  }

  onSelectFac(faculty: FacultyDO): void {
    this.selectedFaculty = faculty;
    this.getAllModules();
  }

  onSelectMod(module: ModulesDO): void {
    this.selectedModule = module;
    this.getAllGroups();
  }

  clickToCreate(): void {
    this.createGroup();
  }

  getAllFaculties() {
    this.route.params.subscribe((params)=> {
      this.dataService.getFacultyData().then((response) => {
        this.faculties = response.payload;
      })
    })
  }

  getAllModules() {
    this.route.params.subscribe((params)=> {
      this.dataService.getModulesData().then((response) => {
        this.modules = response.payload;
      })
    })
  }

  getAllGroups() {
    this.route.params.subscribe((params)=> {
      this.dataService.getGroupData().then((response) => {
        this.groups = response.payload;
      })
    })
  }

  createGroup() {
    let newID = this.modules.length + 1;
    let moduleId = this.selectedModule.id;
    this.newGroup = new GroupDO();
    this.newGroup.id = newID;
    this.newGroup.date = '';
    this.newGroup.status = '';
    this.newGroup.modulid = moduleId;

    this.dataService.createGroupData(this.newGroup).then((group) => {
      this.createdGroup = group.payload;
      this.router.navigateByUrl('http://localhost:8080/groups/new', {state: {group: this.createdGroup}});
    }, (error) => {
      if(error.type === ResponseType.CONNECTION_PROBLEM) {
        this.errorMessage = 'Connection Problem';
      } else {
        this.errorMessage = 'Unknown error happened!';
        }
      });
  }
}
