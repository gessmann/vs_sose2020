import {Component, OnInit} from '@angular/core';
import { DataProviderService } from '../../services/data-provider.service';
import { FacultyDO } from '../../types/facultyDO';
import { ModulesDO } from '../../types/modulesDO';
import { ActivatedRoute } from "@angular/router";
import {GroupDO} from "../../types/groupDO";

@Component({
  selector: 'app-group-search',
  templateUrl: './group-search.component.html',
  styleUrls: ['./group-search.component.css']
})
export class GroupSearchComponent implements OnInit {

  faculties: FacultyDO[];
  modules: ModulesDO[];
  groups: GroupDO[];
  selectedModule:ModulesDO;
  selectedFaculty:FacultyDO;
  selectedGroup: GroupDO;

  constructor(private route: ActivatedRoute,private dataService: DataProviderService) {}

  ngOnInit(): void {
    this.getAllFaculties();
  }

  onSelectFac(faculty: FacultyDO): void {
    this.selectedFaculty = faculty;
    this.getAllModules();
  }

  onSelectMod(module: ModulesDO): void {
    this.selectedModule = module;
    this.getAllGroups();
  }

  onSelectGroup(groups: GroupDO): void {
    this.selectedGroup = groups;
  }

  getAllFaculties() {
    this.route.params.subscribe((params)=> {
      this.dataService.getFacultyData().then((response) => {
        this.faculties = response.payload;
      })
    })
  }

  getAllModules() {
      this.route.params.subscribe((params)=> {
        this.dataService.getModulesData().then((response) => {
        this.modules = response.payload;
     })
    })
  }

  getAllGroups() {
    this.route.params.subscribe((params)=> {
      this.dataService.getGroupData().then((response) => {
        this.groups = response.payload;
      })
    })
  }

}
