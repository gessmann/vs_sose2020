import {Component, OnInit} from '@angular/core';
import {FacultyDO} from "../../types/facultyDO";
import {ModulesDO} from "../../types/modulesDO";
import {GroupDO} from "../../types/groupDO";
import {ActivatedRoute} from "@angular/router";
import {DataProviderService} from "../../services/data-provider.service";

@Component({
  selector: 'app-user-own-groups-page',
  templateUrl: './user-own-groups.component.html',
  styleUrls: ['./user-own-groups.component.css']
})
export class UserOwnGroupsComponent implements OnInit {

  groups: GroupDO[];

  constructor(private route: ActivatedRoute,private dataService: DataProviderService) {}

  ngOnInit(): void {
    this.getAllGroups();
  }

  getAllGroups() {
    this.route.params.subscribe((params)=> {
      this.dataService.getGroupData().then((response) => {
        this.groups = response.payload;
      })
    })
  }

}
