import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOwnGroupsComponent } from './user-own-groups.component';

describe('UserOwnGroupsPageComponent', () => {
  let component: UserOwnGroupsComponent;
  let fixture: ComponentFixture<UserOwnGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserOwnGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOwnGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
