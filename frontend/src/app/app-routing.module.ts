import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupSearchComponent } from './components/group-search/group-search.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { CreateGroupComponent } from './components/create-group/create-group.component';
import { UserOwnGroupsComponent } from './components/user-own-groups/user-own-groups.component';

const routes: Routes = [
  { path: 'user-own-groups', component: UserOwnGroupsComponent },
  { path: 'group-search', component: GroupSearchComponent },
  { path: 'user-login', component: UserLoginComponent },
  { path: 'create-group', component: CreateGroupComponent },
  { path: '', redirectTo: 'user-login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
