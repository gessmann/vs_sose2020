import { ResponseType } from './response-type';

export interface RequestResponse<T> {
  type: ResponseType;
  payload?: T,
}
