package thinkTogether.groupManagement.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import thinkTogether.groupManagement.model.Fakultaet;
import thinkTogether.groupManagement.repo.FakultaetRepository;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FakultaetRestController {

    private final FakultaetRepository fakultaetRepository;

    public FakultaetRestController(FakultaetRepository fakultaetRepository) { this.fakultaetRepository = fakultaetRepository; }

    @GetMapping("/faculties")
    public List<Fakultaet> findAll() { return fakultaetRepository.findAll(); }
}

