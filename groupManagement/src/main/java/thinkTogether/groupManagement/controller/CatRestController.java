package thinkTogether.groupManagement.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import thinkTogether.groupManagement.model.Cat;
import thinkTogether.groupManagement.repo.CatRepository;

import java.util.List;

@RestController
public class CatRestController {

    private final CatRepository catRepository;

    public CatRestController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("/cats")
    public List<Cat> getAllCats() {
        return catRepository.findAll();
    }
}
