package thinkTogether.groupManagement.controller;

import org.springframework.web.bind.annotation.*;
import thinkTogether.groupManagement.model.Learninggroup;
import thinkTogether.groupManagement.repo.GroupRepository;

import java.time.LocalDate;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")

// Verwendet das Repository und gibt die Daten zurück
public class GroupManagementController {

    private final GroupRepository groupRepository;

    public GroupManagementController(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }


    @GetMapping("/")
    public String index() {
        return "Hello from the GroupManagement Controller!";
    }

    @GetMapping("/groups")
    public List<Learninggroup> findAll() {
        return groupRepository.findAll();
    }

    @PostMapping("/groups/new")
    public Learninggroup save(@RequestBody Learninggroup group){
        groupRepository.save(group);
        group.setDatum(LocalDate.now());
        group.setStatus("open");

        return group;
    }



//    /**
//     * e.g. http://localhost:8080/groups/2/
//     *
//     * @param id
//     * @return
//     */
//    @GetMapping(value = "/groups/{id}/")
//    public Group findByID(
//            @PathVariable(value = "id") long id
//    ) {
//        final Group group = groupRepository.findByID(id);
//
//        return group;
//    }
}
