package thinkTogether.groupManagement.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import thinkTogether.groupManagement.model.Modul;
import thinkTogether.groupManagement.repo.ModulRepository;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ModulRestController {

    private final ModulRepository modulRepository;

    public ModulRestController(ModulRepository modulRepository) {
        this.modulRepository = modulRepository;
    }

    @GetMapping("/module")
    public List<Modul> getAllModule() {
        return modulRepository.findAll();
    }
}
