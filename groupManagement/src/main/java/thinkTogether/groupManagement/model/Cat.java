package thinkTogether.groupManagement.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Cat {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private Date date;

    public Cat() {
    }

    public Cat(String name, Date date) {

        this.name = name;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
