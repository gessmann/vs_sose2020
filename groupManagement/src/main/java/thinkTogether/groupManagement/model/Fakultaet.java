package thinkTogether.groupManagement.model;

import javax.persistence.*;

@Entity
@Table(name = "fakultaet")
public class Fakultaet {

    private static final int serialVersion = -1230;
    @Id
    @GeneratedValue
    private int ID;

    @Column(name = "Titel")
    private String titel;

    protected Fakultaet(){
    }

    public Fakultaet(String titel){
        this.titel=titel;
    }

    public void setID(Integer id) { this.ID = id; }
    public Integer getId() {
        return ID;
    }
    public String getTitel() {
        return titel;
    }
    public void setTitel(String datum) {
        this.titel = datum;
    }


    public String toJSON() {
        return "Fakultaet{"+
                ", ID= '" + ID + '\'' +
                ", Titel='" + titel + '\'' +
                '}';
    }
}
