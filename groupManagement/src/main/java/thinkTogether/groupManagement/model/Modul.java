package thinkTogether.groupManagement.model;

import javax.persistence.*;

@Entity
public class Modul {
    @Id
    private Long id;

    private String titel;

    protected Modul() {
    }

    public Modul(String titel) {
        this.titel = titel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }
}