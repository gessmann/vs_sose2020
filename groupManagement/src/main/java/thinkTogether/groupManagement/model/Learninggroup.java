package thinkTogether.groupManagement.model;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;

@Entity
public class Learninggroup {
    @Id
    private Long id;

    private String status;

    private LocalDate date;

    private int modulid;

    protected Learninggroup() {
    }

    public Learninggroup(String status, LocalDate erstelldatum, int modulid) {
        this.date = erstelldatum;
        this.modulid = modulid;
        this.status = status;

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDatum() {
        return date;
    }

    public void setDatum(LocalDate datum) {
        this.date = datum;
    }

    public Integer getModul() {
        return modulid;
    }

    public void setModul(Integer id) {
        this.modulid = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}



