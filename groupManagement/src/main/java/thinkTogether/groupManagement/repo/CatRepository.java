package thinkTogether.groupManagement.repo;


import org.springframework.data.repository.CrudRepository;
import thinkTogether.groupManagement.model.Cat;

import java.util.List;

public interface CatRepository extends CrudRepository<Cat, Long> {
    List<Cat> findAll();
}

