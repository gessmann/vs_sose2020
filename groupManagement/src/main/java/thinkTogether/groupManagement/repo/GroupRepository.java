package thinkTogether.groupManagement.repo;


import org.springframework.data.repository.CrudRepository;
import thinkTogether.groupManagement.model.Learninggroup;

import java.util.List;

//Erstellt Zwischenspeicher für die Datenentitäten? "DAO"???
public interface GroupRepository extends CrudRepository<Learninggroup, Long> {
    public List<Learninggroup> findAll();
    Learninggroup save(Learninggroup group);

}

