package thinkTogether.groupManagement.repo;

import org.springframework.data.repository.CrudRepository;
import thinkTogether.groupManagement.model.Cat;
import thinkTogether.groupManagement.model.Fakultaet;

import java.util.List;

public interface FakultaetRepository extends CrudRepository<Fakultaet, Long> {
    List<Fakultaet> findAll();
}
