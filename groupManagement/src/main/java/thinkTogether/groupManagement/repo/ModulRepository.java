package thinkTogether.groupManagement.repo;

import org.springframework.data.repository.CrudRepository;
import thinkTogether.groupManagement.model.Modul;

import java.util.List;

public interface ModulRepository extends CrudRepository<Modul, Long> {
    List<Modul> findAll();

}