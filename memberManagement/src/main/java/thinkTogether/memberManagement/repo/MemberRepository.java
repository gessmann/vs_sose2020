package thinkTogether.memberManagement.repo;


import org.springframework.data.repository.CrudRepository;
import thinkTogether.groupManagement.model.Learninggroup;

import java.util.List;

//Erstellt Zwischenspeicher für die Datenentitäten? "DAO"???
public interface IGroupRepository extends CrudRepository<Learninggroup, Long> {
    public List<Learninggroup> findAll();

}

