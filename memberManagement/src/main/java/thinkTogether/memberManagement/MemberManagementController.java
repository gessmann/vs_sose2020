package thinkTogether.memberManagement;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import thinkTogether.groupManagement.model.Learninggroup;
import thinkTogether.groupManagement.repo.IGroupRepository;

import java.util.List;


@RestController
// Verwendet das Repository und gibt die Daten zurück
public class GroupManagementController {

    private final IGroupRepository groupRepository;

    public GroupManagementController(IGroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }


    @GetMapping("/")
    public String index() {
        return "Hello from the GroupManagement Controller!";
    }

    @GetMapping("/groups")
    public List<Learninggroup> findAll() {
        return groupRepository.findAll();
    }



//    /**
//     * e.g. http://localhost:8080/groups/2/
//     *
//     * @param id
//     * @return
//     */
//    @GetMapping(value = "/groups/{id}/")
//    public Group findByID(
//            @PathVariable(value = "id") long id
//    ) {
//        final Group group = groupRepository.findByID(id);
//
//        return group;
//    }
}
